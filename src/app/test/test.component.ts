import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TesterService } from '../tester.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  signInForm = new FormGroup({
    UserName: new FormControl('',Validators.required),
    Password: new FormControl('',Validators.required),
  });
  constructor(private testerService:TesterService) { }

  ngOnInit() {
      this.testerService.onSignInOK.subscribe(data=>console.log(data))

      this.testerService.onSignInResponsInvalidUserNameOrPassword.subscribe(data=>console.log("Enter valid username and password"))

      this.testerService.onResponseError.subscribe(error=>console.log(error))


  }
  onSubmit(){
    this.testerService.SignIn({Login:this.signInForm.value})

  }
  handleFileInput(file){
    console.log(file[0])
    this.testerService.UploadFile(file[0])
    .subscribe(response=>console.log("Go it "),
    error=>console.log(error))
  }

}
