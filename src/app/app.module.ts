import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { TestComponent } from './test/test.component'
import {ReactiveFormsModule} from '@angular/forms';
import { TestWebSocketComponent } from './test-web-socket/test-web-socket.component'

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    TestWebSocketComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
