import { Injectable } from '@angular/core';
import {webSocket, WebSocketSubject} from 'rxjs/webSocket'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WSService {
  _subject:WebSocketSubject<unknown>
  constructor() {

   
   }
   Start(){
     console.log("Service Start")
    this._subject = webSocket(
      {url:"wss://localhost:44327/ws?id=1",deserializer: msg => msg})
    this._subject.pipe(
      map((response:any)=>JSON.parse(response.data).Code)
    ).
    subscribe(code=>console.log(code))

   }

}
