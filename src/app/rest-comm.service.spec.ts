import { TestBed } from '@angular/core/testing';

import { RestCommService } from './rest-comm.service';

describe('RestCommService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RestCommService = TestBed.get(RestCommService);
    expect(service).toBeTruthy();
  });
});
