import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestWebSocketComponent } from './test-web-socket.component';

describe('TestWebSocketComponent', () => {
  let component: TestWebSocketComponent;
  let fixture: ComponentFixture<TestWebSocketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestWebSocketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestWebSocketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
