import { Component, OnInit } from '@angular/core';
import { WSService } from '../ws.service';

@Component({
  selector: 'app-test-web-socket',
  templateUrl: './test-web-socket.component.html',
  styleUrls: ['./test-web-socket.component.css']
})
export class TestWebSocketComponent implements OnInit {

  constructor(private wsService:WSService) { }

  ngOnInit() {
   
  }
  BtnClick(){
    console.log("Clicked Start")
    this.wsService.Start()
  }

}
