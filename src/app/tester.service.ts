import { Injectable } from '@angular/core';
import { RestCommService } from './rest-comm.service';
import { Subject } from 'rxjs';
import { SignInRequest } from './sign-in-request';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TesterService {

  ResponseSubjects:{[responseID:string]:Subject<any>}={
    SignInResponseOK:new Subject<any>(),
    SignInInvalidUserNameOrPasswordResponse:new Subject<any>()
  }
  constructor(private commService:RestCommService) {

   }
   get onSignInOK(){
        return this.ResponseSubjects.SignInResponseOK
   }

   get onSignInResponsInvalidUserNameOrPassword(){
    return this.ResponseSubjects.SignInInvalidUserNameOrPasswordResponse
   }
   get onResponseError(){
     return new Subject<any>()
   }

   SignIn(request:SignInRequest){
     return this.commService.SignIn(request).
     pipe(
       map(data=>[data,this.ResponseSubjects[data.responseType]])
     ).
     subscribe(
       ([data,subject])=>subject.next(data),
       error=>console.log("====>>>>",error)
     )
   }
   UploadFile(file:any){
     return this.commService.UploadFile(file)
     
   }



}
