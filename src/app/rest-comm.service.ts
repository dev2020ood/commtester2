import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {Subject,Observable} from 'rxjs'
import { SignInRequest } from './sign-in-request';
import { Response } from './server-response';
import {map} from 'rxjs/operators'
 
@Injectable({
  providedIn: 'root'
})
export class RestCommService {
  
  constructor(private http:HttpClient) { }

  SignIn(request:SignInRequest):Observable<any>{

      return this.http.post("api/SignIn/SignIn",request);

     

  }
  UploadFile(file:any):Observable<any>{
    let headers = new HttpHeaders();
    headers.append("content-type","undefined")
    var formData = new FormData();

    formData.append("file", file, file.name)
    
    let options = { headers: headers };
    console.log(formData)
    console.log(file.size)
    return this.http.post("api/UploadFile",formData,{headers:headers});
  }

}
